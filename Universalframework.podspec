Pod::Spec.new do |s|
    s.name              = 'Universalframework'
    s.version           = '1.0.0'
    s.summary           = 'A really cool SDK that logs stuff.'
    s.homepage          = 'https://gitlab.com/rajasekhar.pattem/universalframework'

    s.author            = { 'Name' => 'rajasekhar.pattem@tarams.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }
    s.ios.deployment_target = '11.0'
    s.swift_version    = '5.0'
    s.platform          = :ios
    s.source            = { :git => 'https://gitlab.com/rajasekhar.pattem/universalframework.git' }
    s.ios.deployment_target = '8.0'
    s.ios.vendored_frameworks = 'Framework/Universalframework.framework'
end
